//  Author Cosmo Kramer

var thisId = function(id){ return document.getElementById(id); },
    divNum = 1,
    styleString,
    lineWidth,
    divCreate,
    dist,
    dur,
    headerStyles,
    animLoop
    css = document.createElement('style'),
    quant = 0,
    ac = 0,
    lineColor = ["#6400aa", "#7c4bb6", "#b70f84", "b17fd4"],  //  line colours
    animTimer = setInterval(function(){ frameAnim(); }, 200);

css.type = 'text/css';
document.getElementsByTagName("head")[0].appendChild(css);  //  create the style tag

function createAnim(){
    divCreate = document.createElement('div'),
    lineWidth = Math.floor((Math.random() * 120) + 10),  //  width of lines created
    colorNum = Math.floor((Math.random() * 3) + 0),  //  random colour number
    dist = Math.floor((Math.random() * 300) + 100),  //  random anim distance
    dur = Math.floor((Math.random() * (9 - 4)) + 4),  //  random anim duration
    divCreate.id = 'line-' + divNum;  //  add line elements
    thisId('wrapper').appendChild(divCreate);  //  append the line element
    styleString = 'width:' + lineWidth + 'px; background:' + lineColor[colorNum] + '; left:-' + lineWidth + 'px; transition: all 0.' + dur + 's;';
    thisId('line-' + divNum).style.cssText = styleString;  //  inline styles
    headerStyles = '#line-' + divNum + '{ opacity:0.8; height: 250px; position: absolute; z-index:100; top: 0; animation-name: keyLine-' + divNum + '; animation-duration: 0.' + dur + 's; animation-timing-function: linear; animation-fill-mode: forwards;animation-iteration-count: 1; } @keyframes keyLine-' + divNum + ' { 0%{ left: ' + lineWidth + 'px; width:' + lineWidth + 'px; } 80%{ left: ' + dist + 'px; width:' + lineWidth + 'px; } 100%{ left: ' + dist + 'px; width: 0px; } }';  //  header styles
    css.appendChild(document.createTextNode(headerStyles));
    divNum++;
    if(divNum > quant){ clearInterval(animLoop); }  //  clear the interval
}

function runAnim(){
    quant = quant + 6;  //  number of lines increment
    animLoop = setInterval(function(){ createAnim(); }, 100);
}

//  frame animations

function frameAnim(){
    ac++;
    switch(ac) {
        //  10 represents a single second
        case 30:
            thisId('frame-1').setAttribute('class', 'right-out');
            runAnim();
            break;
        case 31:
            thisId('frame-2').setAttribute('class', 'right-in');
            break;
        case 60:
            thisId('frame-2').setAttribute('class', 'right-out');
            runAnim();
            break;
        case 61:
            thisId('frame-3').setAttribute('class', 'right-in');
            break;
        case 90:
            thisId('frame-3').setAttribute('class', 'right-out');
            runAnim();
            break;
        case 91:
            thisId('frame-4').setAttribute('class', 'right-in');
            break;
        case 60:
            clearInterval(animTimer);
            break;
        default:
            break;
    }
}

frameAnim();
