//  Author Cosmo Kramer

var thisId = function(id){ return document.getElementById(id); },
    divNum = 1,
    styleString,
    lineWidth,
    lineColor = ["#6400aa", "#7c4bb6", "#b70f84", "b17fd4"],  //  line colours
    divCreate,
    dist,
    dur,
    headerStyles,
    quant = 5,  //  number of lines
    css = document.createElement('style'),
    animTimer = setInterval(function(){ createAnim(); }, 100);

css.type = 'text/css';
document.getElementsByTagName("head")[0].appendChild(css);

function createAnim(){
    divCreate = document.createElement('div'),
    lineWidth = Math.floor((Math.random() * 220) + 10),  //  width of lines created
    colorNum = Math.floor((Math.random() * 3) + 0),  //  random colour number
    dist = Math.floor((Math.random() * 650) + 400),  //  random anim distance
    dur = Math.floor((Math.random() * 4) + 1),  //  random anim duration
    //  add line elements
    divCreate.id = 'line-' + divNum;
    thisId('wrapper').appendChild(divCreate);
    //  inline styles
    styleString = 'width:' + lineWidth + 'px; background:' + lineColor[colorNum] + '; left:-' + lineWidth + 'px; transition: all 0.' + dur + 's;';
    thisId('line-' + divNum).style.cssText = styleString;
    //  header styles
    headerStyles = '#line-' + divNum + '{ opacity:0.8; height: 250px; position: absolute; top: 0; animation-name: keyLine-' + divNum + '; animation-duration: 1.' + dur + 's; animation-timing-function: linear; animation-fill-mode: forwards;animation-iteration-count: 1; } @keyframes keyLine-' + divNum + ' { 0%{ left: ' + lineWidth + 'px; width:' + lineWidth + 'px; } 75%{ left: ' + dist + 'px; width:' + lineWidth + 'px; } 100%{ left: ' + dist + 'px; width: 0px; } }';
    css.appendChild(document.createTextNode(headerStyles));
    divNum++;
    if(divNum > quant){ clearInterval(animTimer); }
}
